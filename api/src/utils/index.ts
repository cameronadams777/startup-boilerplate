/**
 * Return a random number between a minimum value
 * and a maximum value.
 * @param min - minimum value
 * @param max - maximum value
 */
export const randomBetween = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
