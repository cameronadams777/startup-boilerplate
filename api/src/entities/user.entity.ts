import { DateTime } from "luxon";
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from "typeorm";

@Entity("user")
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn("increment") id: number;

  @Column({
    type: "varchar",
    length: 255,
    precision: 255,
    nullable: false
  })
  firstName: string;

  @Column({
    type: "varchar",
    length: 255,
    precision: 255,
    nullable: false
  })
  lastName: string;

  @Column({
    type: "varchar",
    length: 255,
    precision: 255,
    nullable: false
  })
  email: string;

  @Column({
    type: "varchar",
    length: 255,
    precision: 255,
    nullable: false
  })
  firebaseUserUid: string;

  @Column({
    type: "boolean",
    default: false,
    nullable: false
  })
  isAdmin: boolean;

  @Column({
    type: "integer",
    nullable: false,
    default: DateTime.utc().toSQLDate()
  })
  createdAt: string;

  @Column({
    type: "integer",
    nullable: true
  })
  updatedAt: string;

  @Column({
    type: "integer",
    nullable: true
  })
  deletedAt: string;
}
