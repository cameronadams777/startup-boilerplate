import { NestFactory } from "@nestjs/core";
import * as Sentry from "@sentry/node";
import * as firebaseAdmin from "firebase-admin";
import { AppModule } from "./app.module";
import * as developmentServiceAccountConfig from "./config/development-service-account.json";
import * as productionServiceAccountConfig from "./config/production-service-account.json";

function initializeFirebaseAdmin() {
  firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(
      (process.env.NODE_ENV === "production"
        ? productionServiceAccountConfig
        : developmentServiceAccountConfig) as firebaseAdmin.ServiceAccount
    )
  });
}

async function bootstrap() {
  initializeFirebaseAdmin();
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    origin: JSON.parse(process.env.APP_URL!)
  });
  Sentry.init({
    dsn: process.env.DSN,
    environment: process.env.NODE_ENV,
    maxValueLength: 2500
  });
  await app.listen(3000);
}
bootstrap();
