import { HttpException, HttpStatus, UseInterceptors } from "@nestjs/common";
import { Args, Mutation, Resolver } from "@nestjs/graphql";
import firebaseAdmin from "firebase-admin";
import { UserEntity } from "src/entities/user.entity";
import {
  LoginInput,
  LoginPayload,
  LogoutPayload,
  RegistrationInput,
  RegistrationPayload
} from "src/graphql";
import { SentryInterceptor } from "src/interceptors/sentry-interceptor";
import { UsersService } from "src/users/users.service";
import { AuthService } from "./auth.service";

@UseInterceptors(new SentryInterceptor())
@Resolver()
export class AuthResolver {
  constructor(
    private userService: UsersService,
    private authService: AuthService
  ) {}

  @Mutation()
  async login(@Args("input") input: LoginInput): Promise<LoginPayload> {
    const decodedToken = await firebaseAdmin
      .auth()
      .verifyIdToken(input.idToken);

    let user: UserEntity | undefined;

    user = await this.userService.findUser({
      firebaseUserUid: decodedToken.uid
    });

    if (user == null) {
      if (decodedToken.email == null) {
        throw new HttpException(
          "No user found.",
          HttpStatus.INTERNAL_SERVER_ERROR
        );
      }
      user = await this.userService.findUser({ email: decodedToken.email });

      if (user == null) {
        throw new HttpException(
          "This email does not exist",
          HttpStatus.INTERNAL_SERVER_ERROR
        );
      }

      await this.userService.updateUser(user, {
        firebaseUserUid: decodedToken.uid
      });
    }
    const token = this.authService.createToken(user);
    return { token };
  }

  @Mutation()
  async register(
    @Args("input") input: RegistrationInput
  ): Promise<RegistrationPayload> {
    try {
      const decodedToken = await firebaseAdmin
        .auth()
        .verifyIdToken(input.idToken);

      const { firstName, lastName } = input;
      const userChanges: Partial<UserEntity> = {
        firstName,
        lastName,
        firebaseUserUid: decodedToken.uid
      };

      if (decodedToken.email != null) {
        userChanges.email = decodedToken.email;
      }

      let user: UserEntity;

      // first see if there's an existing user by the firebaseUserUid
      user = await this.userService.findUser({
        firebaseUserUid: decodedToken.uid
      });

      if (user == null) {
        // fallback to finding the user by email
        if (decodedToken.email != null) {
          user = await this.userService.findUser({ email: decodedToken.email });
        }
      }

      if (user != null) {
        await this.userService.updateUser(user, userChanges);
      } else {
        if (userChanges.email == null) {
          throw new HttpException(
            "Email must be provided to register.",
            HttpStatus.INTERNAL_SERVER_ERROR
          );
        }
        user = await this.userService.createUser(userChanges);
      }

      const token = await this.authService.createToken(user);
      return { token };
    } catch (error) {
      throw new HttpException(
        "Something went wrong",
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  @Mutation()
  logout(): LogoutPayload {
    return {
      success: true,
      token: undefined
    };
  }
}
