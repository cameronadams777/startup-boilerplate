import { Injectable } from "@nestjs/common";
import * as jwt from "jsonwebtoken";
import { UserEntity } from "src/entities/user.entity";

@Injectable()
export class AuthService {
  createToken(user: UserEntity): string {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return jwt.sign({ ...user }, process.env.JWT_SECRET!, {
      expiresIn: "7d"
    });
  }
}
