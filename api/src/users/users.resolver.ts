import { UseGuards, UseInterceptors } from "@nestjs/common";
import { Args, Context, Mutation, Query, Resolver } from "@nestjs/graphql";
import { pick } from "lodash";
import { UserEntity } from "src/entities/user.entity";
import { UpdateProfileInput } from "src/graphql/resolver-types/schema.gen";
import { SentryInterceptor } from "src/interceptors/sentry-interceptor";
import { AuthGuard } from "../guards/auth.guard";
import { UsersService } from "./users.service";

@UseInterceptors(new SentryInterceptor())
@Resolver("User")
export class UsersResolver {
  constructor(private usersService: UsersService) {}

  @Query()
  @UseGuards(new AuthGuard())
  async me(@Context("user") user: UserEntity): Promise<UserEntity> {
    const userById = await this.usersService.findUser({ id: user.id });
    return userById;
  }

  @Mutation()
  @UseGuards(new AuthGuard())
  async updateProfile(
    @Context("user") user: UserEntity,
    @Args("input") input: UpdateProfileInput
  ): Promise<UserEntity | undefined> {
    const changes = pick(input, [
      "firstName",
      "lastName",
      "email",
    ]) as Partial<UserEntity>;
    return this.usersService.updateUser(user, changes);
  }
}
