import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserEntity } from "../entities/user.entity";

interface FindUsersCriteria {
  id?: number;
  firebaseUserUid?: string;
  email?: string;
}

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>
  ) {}

  private async _findUsers(criteria: FindUsersCriteria): Promise<UserEntity[]> {
    return this.userRepo.createQueryBuilder("user").where(criteria).getMany();
  }

  async findUsers(criteria: FindUsersCriteria): Promise<UserEntity[]> {
    return this._findUsers(criteria);
  }

  async findUser(criteria: FindUsersCriteria): Promise<UserEntity> {
    const users = await this._findUsers(criteria);
    return users?.[0] ?? null;
  }

  async updateUser(
    user: UserEntity,
    changes: Partial<UserEntity>
  ): Promise<UserEntity | undefined> {
    await this.userRepo.update({ id: user.id }, changes);
    return this.userRepo.findOne({ id: user.id });
  }

  createUser({
    firstName,
    lastName,
    email,
    firebaseUserUid
  }: Partial<UserEntity>): Promise<UserEntity> {
    return this.userRepo
      .create({ firstName, lastName, email, firebaseUserUid })
      .save();
  }
}
