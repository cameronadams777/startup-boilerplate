const _path = require('path');
module.exports = {
  babelInclude: [
    _path.resolve(__dirname, './shared/client'),
    _path.resolve(__dirname, './shared/models'),
    _path.resolve(__dirname, "./shared/schema"),
  ],
  babelExclude: [/[\\/]node_modules[\\/]/, /\.d\.tsx?$/],
  moduleResolverAliases: {
    "@shared/client/*": _path.resolve(__dirname, "./shared/client"),
    "@shared/models/*": _path.resolve(__dirname, "./shared/models"),
    "@shared/schema/*": _path.resolve(__dirname, "./shared/schema"),
  },
}