import Fileish from "./Fileish";

export interface FileishReactNative {
  uri: string;
  type?: string;
  name?: string;
}

declare const Upload: unique symbol;
type Upload = typeof Upload | Fileish | FileishReactNative;
export default Upload;
