interface Fileish {
    readonly size: number;
    readonly type: string;
    readonly lastModified?: number;
    readonly name?: string;
    arrayBuffer(): Promise<ArrayBuffer>;
    slice(start?: number, end?: number, contentType?: string): Fileish;
    stream(): unknown;
    text(): Promise<string>;
  }
  
  export default Fileish;
  