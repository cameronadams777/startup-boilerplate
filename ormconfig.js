const ENV = process.env.NODE_ENV || "development";

const baseConfig = {
  type: "postgres",
  database: "{dbname}",
  port: 5432,
  synchronize: false,
  logging: false,
  migrationsRun: false,
  entities: ["dist/**/*.entity.js"],
  migrations: ["dist/migration/**/*.js"],
  cli: {
    entitiesDir: "src/entity",
    migrationsDir: "src/migration",
  },
};

const dynamicConfig = {
  development: {
    host: "localhost",
    database: "{dbname}",
    username: "{dbusername}",
    password: "{dbpassword}",
  },
  staging: {
    host: "{staging-host}",
    database: "{dbname}",
    username: "{dbusername}",
    password: "{dbpassword}",
  },
  production: {
    host: "{production-host}",
    database: "{dbname}",
    username: "{dbusername}",
    password: "{dbpassword}",
  },
};

module.exports = {
  ...baseConfig,
  ...dynamicConfig[ENV],
};
