import React from "react";
import "./App.css";

function App() {
  return (
    <div className="min-h-screen flex justify-center items-center dark:bg-gray-700 dark:text-white">
      <span className="font-bold text-3xl">Lets get things started!</span>
    </div>
  );
}

export default App;
